package com.animushome.heart.packages.examplepackage;


import java.util.Map;

import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.*;
import org.osgi.service.event.EventAdmin;

import com.animushome.heart.service.Area;
import com.animushome.heart.service.DevicePackage;
import com.animushome.heart.service.dal.impl.DevicePackageImpl;

@Component(immediate=true, service=DevicePackage.class)
public class ExamplePackage extends DevicePackageImpl {
	
	//The device registration
	private ServiceRegistration<?> deviceReg;
	
	//The function registration
	private ServiceRegistration<?> funcReg;
	
	/**
	 * Activate is called when the package has all the requirements fulfilled
	 * @param context
	 */
	@Activate
    protected void activate(ComponentContext context) {
      construct("ExamplePackage", context);
    }
	
	/**
	 * Deactivate is called when the package gets uninstalled. We should 
	 * unregister all the services that we have registered.
	 * @param context
	 */
	@Deactivate
    protected void deactivate(ComponentContext context) {
      logger.info("Example package is being deactivated");
      if (deviceReg != null) {
    	  deviceReg.unregister();
      }
      if (funcReg != null) {
    	  funcReg.unregister();
      }
    }
	
	/**
	 * If the device is auto detected it should be registered when this method is called.
	 */
	@Override
	public void scan() {
		if(deviceReg==null){
			logger.info("Found MyDevice");
			MyDevice d = new MyDevice(this);
			deviceReg = registerDevice(d);
			funcReg = registerFunction(new MyFunction(d.DEVICE_UID, this));
			
		} else {
			logger.info("Device is already registered");
		}
		
	}
	
	/**
	 * This method is called when the user wants to add a device manually. 
	 * Any necessary parameters are passed through the Map deviceInfo
	 * 
	 */
	@Override
	public void addDevice(String name, Area area, Map deviceInfo) {
		logger.info("add a new device");
	}
	
	/**
	 * This method is called when the user wants to remove a device. 
	 * Any necessary info (e.g deviceId) for removing the device is passed through
	 * the Map deviceInfo.
	 */
	@Override
	public void removeDevice(Map deviceInfo) {
		logger.info("remove device");
	}
	
	/**
	 * We use Declarative Services to get the EventAdmin so that our 
	 * functions can post events on the event bus when their states changes. 
	 * @param ea
	 */
	@Reference
    void setEventAdmin(EventAdmin ea) {
      this.eventAdmin = ea;
    }

}
