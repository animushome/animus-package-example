package com.animushome.heart.packages.examplepackage;

import com.animushome.heart.service.Package;
import com.animushome.heart.service.dal.DeviceException;
import com.animushome.heart.service.dal.functions.Types;
import com.animushome.heart.service.dal.functions.data.BooleanData;
import com.animushome.heart.service.dal.functions.impl.BooleanControlF;

public class MyFunction extends BooleanControlF {
	private static String FUNCTION_ID = "f1";
	
	public MyFunction(String device_UID, Package p) {
		super(device_UID, FUNCTION_ID, p);
		
		//This boolean controller will switch a Light on/off
		setServiceType(Types.LIGHT);
		
		//Initiate the data with boolean value false
		data = new BooleanData(System.currentTimeMillis(), null, false);
	}

	@Override
	public BooleanData getData() throws DeviceException {
		return data;
	}

	@Override
	public void inverse() throws DeviceException {
		data = new BooleanData(System.currentTimeMillis(), null, !data.value);
	}

	@Override
	public void setData(boolean arg0) throws DeviceException {
		data = new BooleanData(System.currentTimeMillis(), null, arg0);
		
	}

	@Override
	public void setFalse() throws DeviceException {
		data = new BooleanData(System.currentTimeMillis(), null, false);
	}

	@Override
	public void setTrue() throws DeviceException {
		data = new BooleanData(System.currentTimeMillis(), null, true);
	}

}
