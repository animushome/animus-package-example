package com.animushome.heart.packages.examplepackage;

import com.animushome.heart.service.dal.Device;
import com.animushome.heart.service.dal.DeviceStatus;
import com.animushome.heart.service.dal.DeviceTypes;
import com.animushome.heart.service.dal.impl.DeviceImpl;
import com.animushome.heart.service.Package;

public class MyDevice extends DeviceImpl {

	public static final String DEVICE_UID = "com.animushome.heart.packages.examplepackage.mydevice-1";

	public MyDevice(Package p) {
		super(p, DEVICE_UID);
		setProperty(Device.SERVICE_STATUS, DeviceStatus.ONLINE);
	    setProperty(Device.SERVICE_NAME, "My Device 1");
	    setProperty(Device.SERVICE_DESCRIPTION, "A demo device called My device");
	    
	    //Optional properties
	    setProperty(Device.SERVICE_DRIVER, DEVICE_UID + "-driver");
	    setProperty(Device.SERVICE_FIRMWARE_VENDOR, "Anumishome");
	    setProperty(Device.SERVICE_FIRMWARE_VERSION, "0.0.1");
	    setProperty(Device.SERVICE_HARDWARE_VENDOR, "Animushome");
	    setProperty(Device.SERVICE_HARDWARE_VERSION, "0.0.1");
	    setProperty(Device.SERVICE_MODEL, DEVICE_UID + "-model");
	    setProperty(Device.SERVICE_SERIAL_NUMBER, "123-456-789");
	    setProperty(Device.SERVICE_TYPES, new String[] {DeviceTypes.DEVICE});
	}
}
