Example Animus Package
============================================


Example package to get started with Animus Home development. This includes the SDK to develop and deploy your builds easily to your Heart. 

You should use a terminal when running most of these commands. For Windows we recommend https://git-for-windows.github.io/

1. Clone the project with
`git clone ...`

2. Edit your Heart properties. Edit file:
`heart.properties`

3. Start Eclipse and import the cloned Bnd OSGi Workspace
`File->import->general->Existing project into Workspace`

4. Check that the project builds (it will fail because the test case will fail)
`gradle build`

5. Deploy to your Heart
`gradle deploy`


Remove from your Heart
`gradle remove`


For more information read our docs.
https://animushome.atlassian.net/wiki/display/AHK/Developer